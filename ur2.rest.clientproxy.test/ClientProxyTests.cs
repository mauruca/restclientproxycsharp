﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ur2.REST.ClientProxy;

namespace Ur2.REST.ClientProxy.Test
{
    [TestClass]
    public class ClientProxyTests
    {
        [TestMethod]
        public void TestClientProxyGETMetodoPadrao()
        {
            RESTClientProxy cp = new RESTClientProxy();
            Assert.AreEqual(HttpVerb.GET, cp.Method);
        }
    }
}
