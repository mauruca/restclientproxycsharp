﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Ur2.REST.ClientProxy
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    public struct LocalContentType  {
        public const string JSON = "application/json";
        public const string XML = "text/xml";
    }

    public class RESTClientProxy
    {
        public RESTClientProxy()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = LocalContentType.JSON;
            Accept = LocalContentType.JSON;
            PostData = "";
        }

        public RESTClientProxy(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = LocalContentType.JSON;
            Accept = LocalContentType.JSON;
            PostData = "";
        }

        public RESTClientProxy(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = LocalContentType.JSON;
            Accept = LocalContentType.JSON;
            PostData = "";
        }

        public RESTClientProxy(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = LocalContentType.JSON;
            Accept = LocalContentType.JSON;
            PostData = postData;
        }

        public string Accept { get; set; }
        public string ContentType { get; set; }
        private string _endpoint;
        public string EndPoint
        {
            get { return _endpoint;  }
            set { _endpoint = value.Trim();  }
        }
        public HttpVerb Method { get; set; }
        public string PostData { get; set; }

        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters, string name = "", string value = "", HttpStatusCode expectedStatusCode = HttpStatusCode.OK)
        {
            var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

            request.Method = Method.ToString();
            request.ContentLength = 0;
            request.Accept = Accept;
            request.ContentType = ContentType;
            if(name.Trim().Length > 0 && value.Trim().Length > 0)
                request.Headers.Add(name, value);

            if (!string.IsNullOrEmpty(PostData) && (Method == HttpVerb.POST || Method == HttpVerb.PUT))
            {
                var encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                request.ContentLength = bytes.Length;

                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != expectedStatusCode)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();
                    }
                }

                return responseValue;
            }
        }
    }
}
